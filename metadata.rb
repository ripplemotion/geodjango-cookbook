name             'ripple-geodjango-cookbook'
maintainer       'Olivier Tabone'
maintainer_email 'contact@ripplemotion.fr'
license          'MIT'
description      'Setup geodjango'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'