geodjango_pkgs = value_for_platform(
#  ["debian","ubuntu"] => {
#    "default" => ["python","python-dev"]
#  },
#  ["centos","redhat","fedora"] => {
#    "default" => ["python26","python26-devel"]
#  },
#  ["freebsd"] => {
#    "default" => ["python"]
#  },
  "default" => [
	"python-dev",
	"build-essential",
	"libjpeg62-dev",
	"libxml2-dev",
	"libxslt-dev",
	"libpq-dev",
  "libgdal-dev",
  "swig",
  ]
)

geodjango_pkgs.each do |pkg|
  package pkg do
    action :install
  end
end

